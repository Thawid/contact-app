
const db = require('./db');

const register = async ({ username, password}) => {
  const sql = `INSERT INTO users VALUES (null ,?,?)`;
  return await db.query(sql,[username, password]);
};

const contact = async ({ name,nick,dob,userId}) => {

  const sql = `INSERT INTO contacts VALUES (null ,?,?,?,?)`;
  return await db.query(sql, [name,nick,dob,userId]);

};

const emails = async ({ contactId, email}) => {
  const sql = `INSERT INTO emails VALUES (?,?)`;
  return await db.query(sql,[contactId, email]);
}

const searchResult = async ({ userId,search }) =>{
  const sql = `SELECT contacts.name,contacts.nick,contacts.dob,emails.email FROM contacts join emails ON contacts.contactId = emails.contactId WHERE contacts.userId = ${userId} AND contacts.name LIKE '%${search}%'`;
  const result = await db.query(sql);
  return result.length > 0 ? result : null;
}

const getUser = async (id)=>{
  try {
    const getId = `SELECT * FROM users WHERE userId = ${id}`;
    const getUserId = await db.query(getId);
    return getUserId.length > 0 ? getUserId[0]: null;
  }catch (error){
    return null;
  }
}

const getContact = async (id)=>{
  try {
    const contactId = `SELECT * FROM contacts WHERE contactId = ${id}`;
    const getContactId = await db.query(contactId);
    return getContactId.length > 0 ? getContactId[0]: null;
  }catch (error){
    return null;
  }
}


module.exports = {
  register,
  contact,
  getUser,
  getContact,
  emails,
  searchResult

}