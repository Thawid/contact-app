const express = require('express');
const router = express.Router();
const model = require('./model');
const { validateRequest } = require('./validator');
const Response = require('./utils/response');

// require your models here


// sample post method
// router.post('/', async (req, res) => {
//   const response = new Response(res);
//   try {
//     validate request to check user input errors
//     call model methods
//     send response to client
//   } catch (error) {
//     send error response to client
//   }
// });

// register user

router.post('/register', async (req,res) =>{
    const response = new Response(res);

    try {
        const { data, error} = validateRequest(req, 'register');
        if(error) return  response.badRequest(error);
        const { username, password } = data;
        const user = await model.register({username, password});
        const registerPayload = {
            success:'true',
            message:'User registered successfully'
        };
        return response.content(registerPayload);
    }catch (error){
        const message = error.message ? error.message : 'Server error';
        return  response.internalServerError({ message })
    }
});


router.post('/contacts', async (req,res) =>{

    const response = new Response(res);

    try {
        const { data, error } = validateRequest(req,'contact');
        if(error) return response.badRequest(error);
        const { name, nick, dob, userId } = data;
        const user = await model.getUser(userId);
        if(!user) return response.badRequest({ message:'User not found'});
        const contact = await model.contact({ name, nick, dob, userId});

        const  contactPayload = {
            success: 'true',
            message: "Contact added successfully",
            contactId: userId

        };
        return  response.content(contactPayload);

    }catch (error){
        const message = error.message ? error.message : 'Server error';
        return  response.internalServerError({ message })
    }
});


router.post('/contacts/emails', async (req,res) =>{

    const response = new Response(res);

    try {

        const { data, error } = validateRequest(req,'email');
        if(error) return response.badRequest(error);

        const { contactId, email } = data;
        const contact = await model.getContact(contactId);
        if(!contact) return response.badRequest({ message:'Contact not found'});
        await model.emails({ contactId, email });

        const  emailPayload = {
            "success": true,
            "message": "Email added successfully"
        };
        return  response.content(emailPayload);

    }catch (error){
        const message = error.message ? error.message : 'Server error';
        return  response.internalServerError({ message })
    }
});


router.post('/contacts/search',async (req,res) =>{
    const response = new Response(res);
    try {
        const userId = req.body.userId;
        const search = req.body.search;
        const result =  await model.searchResult({userId,search});
        console.log(result);
    }catch (error){
        const message = error.message ? error.message : 'Server error';
        return  response.internalServerError({ message })
    }
});

module.exports = router;