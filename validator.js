const { validate } = require('./utils/request-validator');

const rules = {
  register:{
    username:{ type:'string', empty:false },
    password:{ type: 'string', empty: false},
  },
  contact:{
    name:{ type:'string', empty:false },
    nick:{ type: 'string', optional:true},
    dob: { type: 'string', empty: false, optional: true, pattern: /^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/ },
    userId:{ type: 'number', empty:false, integer:true }
  },
  email:{
    contactId:{ type:'number', empty:false, integer: true},
    email: { type:'string', empty:false }
  }
};

const validateRequest = (req, ruleName) => {
  const rule = rules[ruleName];
  return validate(req, rule);
};

module.exports = {
  validateRequest,
};